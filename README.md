# Remote Control Server

Remote control your PC from your web browser on your other PC or mobile device.

Supports mouse movements, scrolling, clicking and keyboard input.

# Install & Run

Install from npm:

```bash
npm install -g remote-control-server
remote-control-server
```

or use from git source:

```bash
git clone https://bitbucket.org/admira/remotecontrolserver.git
cd remotecontrolserver
npm install
node dist/index.js
```

On your other machine or mobile device open the url:

```bash
http://192.168.0.10:3000
```

Replace `192.168.0.10` with the LAN IP address of your server.

# Note

This package requires [robotjs](https://www.npmjs.com/package/robotjs) so make
sure you have the required prerequisites installed for compiling that package.

# license

Forked from: https://github.com/jeremija/remote-control-server
MIT
