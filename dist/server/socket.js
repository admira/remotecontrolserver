'use strict';

const keyMapping = {
  9: 'tab',
  8: 'backspace',
  13: 'enter',
  27: 'escape',
  37: 'left',
  38: 'up',
  39: 'right',
  40: 'down'
};


module.exports = function(socket, robot) {

  function moveMouse(pos) {
    let mouse = robot.getMousePos();
    if ( !dragToggle ) {
      let x = mouse.x + pos.x;
      let y = mouse.y + pos.y;
      robot.moveMouse(x, y);
    }
    else {
      let brake = 3;
      let x = mouse.x + pos.x/brake;
      let y = mouse.y + pos.y/brake;
      robot.moveMouse(x, y);
    }
  }

  function scrollMouse(pos) {
    var mov = {};
    mov.x = pos.x > 0 ? -1 : 1;
    mov.y = pos.y > 0 ? -1 : 1;
    robot.scrollMouse(mov.y, mov.x);
  }

  socket.on('keytap', key => {
    robot.keyTap(key);
  });

  socket.on('mousemove', (pos, scroll) => {
    if (!scroll) {
      moveMouse(pos);
      return;
    }
    scrollMouse(pos);
  });

  socket.on('click', params => {
    robot.mouseClick(params.button, params.double);
  });

  socket.on('keypress', action => {
    let alt = action.alt;
    let ctrl = action.ctrl;
    let shift = action.shift;
    let meta = action.meta;
    let code = action.code;
    let string = action.string;

    if (alt) robot.keyToggle('alt', 'down');
    if (ctrl) robot.keyToggle('control', 'down');
    if (shift) robot.keyToggle('shift', 'down');
    if (meta) robot.keyToggle('command', 'down');

    let specialKey = keyMapping[code];
    if (string) {
      robot.typeString(string);
    }
    if (specialKey) {
      robot.keyTap(specialKey);
    }

    if (alt) robot.keyToggle('alt', 'up');
    if (ctrl) robot.keyToggle('control', 'up');
    if (shift) robot.keyToggle('shift', 'up');
    if (meta) robot.keyToggle('command', 'up');
    return false;
  });

  socket.on('toggle-key', (key, state) => {
    if (key === 'meta') key = 'command';
    robot.keyToggle(key, state);
  });

  var dragToggle = false;
  socket.on('toggle-button', (button, state) => {
    if ( button === 'left' ){
      dragToggle = state == 'down' ? true : false;
    }
    else {
      robot.mouseToggle(state, button);
    }
  });

  socket.on('log', (msg) => {
    console.log(msg);
  });

  socket.on('touchStart', () => {
    if ( dragToggle) robot.mouseToggle('down', 'left' );
  });

  socket.on('touchEnd', () => {
    if ( dragToggle) robot.mouseToggle('up', 'left' );
  });

};
